var createError = require('http-errors');
var express = require('express');//1
var path = require('path');//2
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mongoose = require('mongoose');//3
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var cryptosRouter = require('./routes/cryptos')

var app = express();

//db connection
mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true})
.then(() =>{
  console.log('Conexion con db exitosa')
})
.catch((err) => {
  console.error('Error con la db papu',err)
})

// view engine setup
//Path
app.set('views', path.join(__dirname, 'views'));
//Views
//-----
//Engine PUG
app.set('view engine', 'pug'); 
//Cosas 1
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
//Port
app.use(express.static(path.join(__dirname, 'public')));
//Index
//-----
//Cosas 2
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/',cryptosRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

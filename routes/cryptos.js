var express = require('express');
var router = express.Router();
var Models = require('../models/cryptos')

router.get('/cryptos', function(req,res){
    Models.find({})
    .exec()
    .then((docs)=>{
        res.json(docs)
    })
    .catch((error)=>
    {
        const message = 'Error en la DB'
        console.log(message, error)
        res.status(500).json({message})
    })
});
router.get('/cryptos/:ticker', function(req, res){
    Models.findById(req.params.ticker)
    .exec()
    .then((docs) =>{
        res.json(docs)
    })
    .catch((error)=>
    {
        const message = 'Error en la db'
        console.log(message, error)
        res.status(500).json({message})
    })
});
router.put('/cryptos/:ticker/:nuevo', function(req, res){
    Models.findByIdandUpdate(req.params.ticker, req.params.nuevo)
    .exec()
    .then((docs) =>{
        res.json(docs)
    })
    .catch((error)=>
    {
        const message = 'Error en la db'
        console.log(message, error)
        res.status(500).json({message})
    })
});
router.post('/cryptos/:ticker/:create', function(req, res){
    Models.create(req.params.ticker, req.params.create)
    .exec()
    .then((docs) =>{
        console.log('Crypto ' + req.params.create + ' creado con exito')

    })
    .catch((error)=>
    {
        const message = 'Error en la db'
        console.log(message, error)
        res.status(500).json({message})
    })
});
router.delete('/cryptos/:ticker', function(req, res){
    Models.findByIdandDelete(req.params.ticker)
    .exec()
    .then((docs) =>{
        console.log('Crypto ' + req.params.ticker + ' borrado con exito')
    })
    .catch((error)=>
    {
        const message = 'Error en la db'
        console.log(message, error)
        res.status(500).json({message})
    })
});


module.exports = router
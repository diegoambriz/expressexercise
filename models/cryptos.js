const mongoose = require('mongoose');

var schema = new mongoose.Schema ({
    tokenName: {
        type:String,
    },
    author: {
        type:String,
    },
    functionality: {
        type:String,
    },
    isAbleToMine:{
        type:Boolean,
    }
})

var model = mongoose.model('Cryptos', schema)
module.exports = model